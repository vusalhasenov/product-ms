package com.example.productms.service;


import com.example.productms.dto.ProductRequest;
import com.example.productms.dto.ProductResponse;

import java.util.List;

public interface ProductService {

    void createProduct(ProductRequest productRequest);

    List<ProductResponse> getAllProducts();
}
