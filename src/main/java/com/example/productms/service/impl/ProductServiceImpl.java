package com.example.productms.service.impl;


import com.example.productms.dto.ProductRequest;
import com.example.productms.dto.ProductResponse;
import com.example.productms.entity.Product;
import com.example.productms.mapper.ProductMapper;
import com.example.productms.repository.ProductRepository;
import com.example.productms.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Override
    public void createProduct(ProductRequest productRequest) {
        Product product = productMapper.mapToEntity(productRequest);
        productRepository.save(product);
        log.info("Product {} is saved",product.getId());

    }

    @Override
    public List<ProductResponse> getAllProducts() {
        List<Product> all = productRepository.findAll();
        return all.stream().map(product -> productMapper.mapToProductResponse(product)).collect(Collectors.toList());
    }
}
