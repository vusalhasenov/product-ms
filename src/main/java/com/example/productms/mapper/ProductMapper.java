package com.example.productms.mapper;


import com.example.productms.dto.ProductRequest;
import com.example.productms.dto.ProductResponse;
import com.example.productms.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ProductMapper {
    
    Product mapToEntity(ProductRequest productRequest);

    ProductResponse mapToProductResponse(Product product);
}
