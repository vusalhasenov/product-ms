package com.example.productms.repository;
import com.example.productms.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {

    @Override
    <S extends Product> S save(S entity);

    @Override
    List<Product> findAll();
}
